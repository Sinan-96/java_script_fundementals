const payButton = document.getElementById("pay");
const workButton = document.getElementById("work");
const bankButton = document.getElementById("bank");
const loanButton = document.getElementById("loan");
const buyButton = document.getElementById("buy");
const repayButton = document.getElementById("repay");

const computerDropdown = document.getElementById("computers");

const payBalanceText = document.getElementById("payBalance");
const bankBalanceText = document.getElementById("bankBalance");
const computerName = document.getElementById("computerName");
const computerDescription = document.getElementById("computerDescription");
const computerPrice = document.getElementById("computerPrice");

const computerPicture = document.getElementById("picture");

const featureList = document.getElementById("features");


let computers = [];
let totalDebt = 0.0;
let currentBankBalance = 0.0;
let currentpayBalance = 0.0;
repayButton.style.visibility = "hidden";

/*Fetches data from the API and fills the computer dropdown
with the fetched data*/

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputersToMenu(computers));


//Adds API data to dropdown
const addComputersToMenu = (computers) => {
    computers.forEach(x => addComputerToMenu(x));
    computerPrice.innerText =selectedComputer.price + " NOK";
}



//Adds specific data element/computer to dropdown
const addComputerToMenu = (computer) =>{
    const computerElement = document.createElement("option");
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));
    computerDropdown.appendChild(computerElement);

}

//Fires when computer changes computer in the dropdown
const handleComputerMenuChange = e => {
    featureList.innerHTML = "";
    const selectedComputer = computers[e.target.selectedIndex];
    computerPrice.innerText =selectedComputer.price + " NOK";
    computerPicture.src = selectedComputer.image;
    computerName.innerText = selectedComputer.title;
    computerDescription.innerText = selectedComputer.description;
    fillFeatures(selectedComputer.specs);
}
//Fills feature text with features of selected computer
const fillFeatures = features => {
    features.forEach(addfeature);
}

//Adds specific feature to feature text
const addfeature = feature =>{
    featureList.innerHTML += feature +", ";
}

/*Fires when user clicks loan button
User most have half the amount he tries to loan already in
the bank*/
function onLoanButtonClick(){
    if(totalDebt > 0.0){
        alert("You already have a loan!");
        return;
    }
    if(currentBankBalance <= 0){
        alert("You dont have enough money to loan anything!");
        return;
    }
    let loan = parseInt(prompt("Please enter loan amount: "));
    if(loan > 2*currentBankBalance){
        alert("You cannot get a loan that is over twice the size of your bank balance!");
    }
    else {
        totalDebt = loan;
        currentBankBalance += loan;
        bankBalanceText.innerText = currentBankBalance.toString() + " NOK";
        repayButton.style.visibility = "visible";
    }
    
    
}

/*Fires when work button is clicked
Adds 100 to users pay balance*/
function onWorkButtonClick(){
    currentpayBalance += 100.0;
    payBalanceText.innerText = currentpayBalance.toString() + " NOK";
}

/*Transfers money from pay balance to bank balance
Fires when abank button is clicked
Also uses 10% to repay loan if there is one*/
function onBankButtonClick(){
    if(totalDebt > 0){
        totalDebt -= 0.1 * currentpayBalance;
        currentpayBalance *= 0.9;
    }
    currentBankBalance += currentpayBalance;
    currentpayBalance = 0.0;
    payBalanceText.innerText = currentpayBalance.toString() + " NOK";
    bankBalanceText.innerText = currentBankBalance.toString() + " NOK";
}
//Repays debt
function onRepayButtonClick(){
    if(totalDebt <= currentpayBalance){
        currentpayBalance -= totalDebt;
        totalDebt = 0;
        repayButton.style.visibility = "hidden";
        

    }

    else{
        totalDebt -= currentpayBalance;
        currentpayBalance = 0;
    }
    payBalanceText.innerText = currentpayBalance.toString() + " NOK";

}
//Buys cimputer
function onBuyButtonClick(){
    price = parseInt(computerPrice.innerText);
    if(currentBankBalance>= price ){
        currentBankBalance -= price;
        alert("You now own " + computerName.innerText + "!");
        bankBalanceText.innerText = currentBankBalance.toString() + " NOK";
    }
    else{
        alert("You cannot afford the computer!")
    }
}

computerDropdown.addEventListener("change",handleComputerMenuChange);
workButton.addEventListener("click",onWorkButtonClick);
bankButton.addEventListener("click",onBankButtonClick);
loanButton.addEventListener("click",onLoanButtonClick);
repayButton.addEventListener("click",onRepayButtonClick);
buyButton.addEventListener("click",onBuyButtonClick);